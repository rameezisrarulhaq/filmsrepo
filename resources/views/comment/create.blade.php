@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-2">
            
            @include('layouts.nav')
        
        </div>


        <div class="col-md-8">
            <div class="card">
               

                <div class="card-body">
                        <h2>Create a Film</h2>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <form method="POST" action="/films" enctype="multipart/form-data">

                      @csrf

                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>
                          <input type="text" required name="name" class="form-control" aria-describedby="emailHelp" placeholder="e.g Jerrasic World">
                        </div>
                      
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Description</label>
                            <textarea required name ="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Release Date</label>
                            <input required type="date" name="release_date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g 12/12/18">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Genre</label>
                           
                            @foreach ($genres as $genre)
                             
                            <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{$genre->id}}" name="genre[]" value="{{$genre->id}}">
                                <label class="custom-control-label genreLabel" for="{{$genre->id}}">{{$genre->name}}</label>
                             </div>

                             @endforeach
                        </div>

                        <div class="form-group">
                                <label for="exampleInputEmail1">Select Rating</label>
                            <select class="custom-select" required name="rating">
                              <option value="">Select</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                          
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ticket Price: $</label>
                            <input required type="text" name="ticket_price" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g 59">
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Country</label>
                            <input required type="text" name="country" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g England">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlFile1">Select a Photo</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="photo">
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    
                    
                </div>

                
             
            

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
