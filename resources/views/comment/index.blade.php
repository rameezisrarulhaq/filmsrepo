@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

            <div class="col-md-2">
                    
                   @include('layouts.nav')
               
            </div>
                
            

        <div class="col-md-9">

            @if (Session::has('warning'))
                <div class="alert alert-warning alert-dismissible wafade show" role="alert">
                  <strong>Oops!</strong>  {{ session('warning') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif

            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible wafade show" role="alert">
                <strong>Success!</strong>  {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            @endif


            <div class="card">
                <div class="card-header">Listing All Films</div>

               

                <div class="row">
                    
                   
                    @foreach ($films as $film)
                            

                        <div class="col-sm-6" style="width: 18rem;">
                            <img class="card-img-top" src="<?php echo asset("storage/$film->photo")?>" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{$film->name}}</h5>
                                <p class="card-text">{{$film->sentence}}</p>
                                <a href="films/{{$film->name}}" class="btn btn-primary">Show Details</a>
                            </div>
                        </div>

                    @endforeach


                </div>

                
             
            

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
