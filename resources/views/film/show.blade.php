@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

            <div class="col-md-2">
                    
                    @include('layouts.nav')
                
             </div>

        <div class="col-md-8">

                @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible wafade show" role="alert">
                  <strong>Congrats!</strong>  {{ session('success') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif


            <div class="card">
            <div class="card-header">{{$film->name}} Details</div>

                <div class="card-body">
                    
                    <div class="card" style="width: 100%">
                        <img class="card-img-top" src="<?php echo asset("storage/$film->photo")?>"  alt="Card image cap">
                        <div class="card-body">
                        <p class="card-text">{{$film->sentence}}</p>

                        <p class="card-text">Release Date: {{$film->release_date}}</p>

                        <p class="card-text">Rating: {{$film->rating}}</p>

                        <p class="card-text">Ticket Price: ${{$film->ticket_price}}.00</p>

                        <p class="card-text">Country: {{$film->country}}</p>
                                
                        <div class="card">
                            <div class="card-body">
                                <p>Genre:</p>
                                
                                    <ul>
                                        @foreach ($genres as $genre)
                                                
                                                <li>{{$genre->name}}</li>
                                            
                                            @endforeach
                                            
                                    </ul>
                                
                            </div> 
                                        
                        </div>
                                
                        <br />

                        <h2>Comments</h2>
                                
                        @if(count($comments) > 0)
                            @foreach ($comments as $comment)
                                        
                                <div class="card" style="width: 100%;">
                                        <div class="card-body">
                                        
                                        <p class="card-text">{{$comment->comment}}</p>
                                        <p href="#" class="card-link"><i>{{$comment->name}}</i></p>
                                        </div>
                                </div>
                                    
                            @endforeach
                            
                            @else
                                    <p>No comments yet posted.</p>
                            @endif
                                
                            <br />
                               
                            <h2>Leave a comment</h2>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                                    
                            <form method="POST" action="/comments">

                                @csrf
                            <input type="hidden" name="id" value="{{$film->id}}"/>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" required name="name" class="form-control" aria-describedby="emailHelp" placeholder="John Doe">
                                </div>
                                
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Comment</label>
                                    <textarea required name ="comment" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>

                        </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
