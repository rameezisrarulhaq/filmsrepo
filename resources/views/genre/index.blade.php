@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

            <div class="col-md-2">
                    
                   @include('layouts.nav')
               
            </div>
                


        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Listing All Genres</div>

                <ul class="list-group list-group-flush">

                
                    
                   
                    @foreach ($genres as $genre)
                            

                    <li class="list-group-item">
                       
                            <div class="card-body">
                        
                                <p class="card-text">{{$genre->name}}</p>
                        
                            </div>
                    </li>

                    @endforeach


                </ul>
                
             
            

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
