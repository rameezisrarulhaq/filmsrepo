@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">{{$film->name}} Details</div>

                <div class="card-body">
                    
                    <div class="card" style="width: 100%">
                                <img class="card-img-top" src="<?php echo asset("storage/$film->photo")?>"  alt="Card image cap">
                                <div class="card-body">
                                <p class="card-text">{{$film->sentence}}</p>

                                <p class="card-text">Release Date: {{$film->release_date}}</p>

                                <p class="card-text">Rating: {{$film->rating}}</p>

                                <p class="card-text">Ticket Price: ${{$film->ticket_price}}.00</p>

                                <p class="card-text">Country: {{$film->country}}</p>
                                
                                </div>
                        </div>

                </div>

                
             
            

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
