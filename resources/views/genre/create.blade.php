@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<div class="container">
    <div class="row justify-content-center">

            <div class="col-md-2">
                    
                    @include('layouts.nav')
                
             </div>

             
        <div class="col-md-8">
            <div class="card">
               

                <div class="card-body">
                        <h2>Create a Genre</h2>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <form method="POST" action="/genres" >

                      @csrf

                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>
                          <input type="text" required name="name" class="form-control" aria-describedby="emailHelp" placeholder="e.g Fantasy">
                        </div>
                      

                        <button type="submit" class="btn btn-primary">Submit</button>
                    
                    </form>

                    
                    
                </div>

                
             
            

            </div>
        </div>
    </div>
</div>
<script  type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@endsection
