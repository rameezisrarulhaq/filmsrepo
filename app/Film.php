<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    
    protected $guarded = ['id'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre');
    }

    public function comments(){

        return $this->hasMany('App\Comment');

    }

}
