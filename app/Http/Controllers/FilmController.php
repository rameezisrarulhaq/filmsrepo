<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Genre;


class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all();
        
        return view('film.index',[
            'films' => $films
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        // make sure atleast one genre exist before letting to excess create a Film

        $genres = Genre::all();

        if(count($genres))
        {
                
            return view('film.create',[
                'genres' => $genres
            ]);
        }
        else 
        {
           
            $request->session()->flash('warning', 'Please create atleast one genre in order to create films');

            return redirect('/films');

        }

        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:films',
            'description' => 'required',
            'release_date' => 'required',
            'rating' => 'required',
            'ticket_price' => 'required',
            'country' => 'required',
            'photo' => 'required',
            'genre' => 'required'

        ]);

       
       
       $photo =  Storage::put('films', $request->photo);
       
        $film = Film::create([
                    'name' => $request->name,
                    'description' => $request->description,
                    'release_date' => $request->release_date,
                    'rating' => $request->rating,
                    'ticket_price' => $request->ticket_price,
                    'country' => $request->country,
                    'photo' => $photo,
                    ]);

       
        $film->genres()->sync($request->genre);

        $request->session()->flash('success', 'Film created successfully!');

        return redirect('/films');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function show(Film $film)
    {   

        $genres =  $film->genres;

        $comments =  $film->comments;

        return view('film.show', [

            'film' => $film ,
            'genres' => $genres,
            'comments' => $comments

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit(Film $film)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Film $film)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        //
    }
}
