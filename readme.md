

## About the App

A simple film application that uses Laravel. 

The app allow the registered users to add genre and films.

The registered users can comment on any film.



## Usage

Clone the repo

Install composer dependencies

Run database migrations

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
