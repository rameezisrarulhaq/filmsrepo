<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',  'FilmController@index')->middleware('auth');

Auth::routes();


Route::resource('films', 'FilmController')->middleware('auth');

Route::resource('genres', 'GenreController')->middleware('auth');

Route::resource('comments', 'CommentController')->middleware('auth');